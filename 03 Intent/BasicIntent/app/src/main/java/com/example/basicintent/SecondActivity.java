package com.example.basicintent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {
    Integer tatol = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent i = getIntent();
        String num1 = i.getStringExtra("num1");
        String num2 = i.getStringExtra("num2");

        TextView value1 = findViewById(R.id.txtVal1);
        TextView value2 = findViewById(R.id.txtVal2);
        value1.setText(num1);
        value2.setText(num2);

        tatol = Integer.parseInt(num1)+Integer.parseInt(num2);
        Toast.makeText(SecondActivity.this,"Total = "+ tatol,Toast.LENGTH_SHORT).show();
    }


    public void BackMain(View view) {
        Intent intent = new Intent();
        intent.putExtra("returnSum",tatol.toString());
        setResult(RESULT_OK,intent);
        super.finish();
    }
}